<?php
/**
 * Sprout Extended plugin for Craft CMS 3.x
 *
 * Extra functionality for Sprout Forms
 *
 * @link      https://www.the-craft.be
 * @copyright Copyright (c) 2018 thecraft
 */

namespace thecraft\sproutextended\services;

use thecraft\sproutextended\SproutExtended;

use Craft;
use craft\base\Component;

/**
 * Reports Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    thecraft
 * @package   SproutExtended
 * @since     0.0.1
 */
class Reports extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     SproutExtended::$plugin->reports->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';
        // Check our Plugin's settings for `someAttribute`
        if (SproutExtended::$plugin->getSettings()->someAttribute) {
        }

        return $result;
    }
}
