/**
 * Sprout Extended plugin for Craft CMS
 *
 * Sprout Extended JS
 *
 * @author    thecraft
 * @copyright Copyright (c) 2018 thecraft
 * @link      https://www.the-craft.be
 * @package   SproutExtended
 * @since     0.0.1
 */
