<?php
/**
 * Sprout Extended plugin for Craft CMS 3.x
 *
 * Extra functionality for Sprout Forms
 *
 * @link      https://www.the-craft.be
 * @copyright Copyright (c) 2018 thecraft
 */

/**
 * Sprout Extended en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('sprout-extended', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    thecraft
 * @package   SproutExtended
 * @since     0.0.1
 */
return [
    'Sprout Extended plugin loaded' => 'Sprout Extended plugin loaded',
];
