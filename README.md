# Sprout Extended plugin for Craft CMS 3.x

Extra functionality for Sprout Forms

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /sprout-extended

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Sprout Extended.

## Sprout Extended Overview

-Insert text here-

## Configuring Sprout Extended

-Insert text here-

## Using Sprout Extended

-Insert text here-

## Sprout Extended Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [thecraft](https://www.the-craft.be)
